# Open University

Open University is an open source platform for providing an e-learning system for simulate university or campus alike experience. For a student, they have an option for taking a long or formal course, which means they need to fulfill some credits before they can graduate for a major or program. Or, taking a short course which commonly for learning new specific skills.

For a lecture or teacher, they can create a class, subject study, material study for each subject, assignment and examination.

For examination features, lectures can set the type of answers. The option would be multiple choice, free text, etc.

For higher-level users such as administrators or university owners, they can set up faculty, program study and major.

Once each student completed a course they will get certificates or even formal degrees.

## ER Diagram

For ER Diagram look on this link https://dbdiagram.io/d/5e5904dc4495b02c3b878d85

## Glossary

* Faculty: A group of university departments concerned with a major division of knowledge. Eg. Faculty of Law, Faculty of Pshychology, Faculty of Computer.
* Programs: Any course or grouping of courses prerequisite to or indicative of a degree.
* Degree level: course, diploma, bachler, master, doctoral
* Subject: A specifc course.
* Classes Room: A room for students and lectures base on a subject.
* Materials study: 
* Exams
* Assignments
* Lectures
* students
* Certificates
