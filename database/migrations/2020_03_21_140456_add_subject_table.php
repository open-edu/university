<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->foreignId('program_id');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreignId('lecture_id');
            $table->foreign('lecture_id')->references('id')->on('lectures');
            $table->char('title');
            $table->text('description')->nullable();
            $table->char('code')->nullable();
            $table->char('category')->nullable();
            $table->foreignId('dependent_subject_id')->nullable();
            $table->smallInteger('credit_hours')->default(0);
            $table->smallInteger('tuition_fees')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
