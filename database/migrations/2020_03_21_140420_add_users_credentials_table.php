<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsersCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_credentials', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->unsignedBigInteger('user_id');
            $table->enum('type', ['password', 'email', 'phone', 'token']);
            $table->enum('status', ['active', 'inactive', 'unverified', 'deleted'])->default('active');
            $table->char('key')->nullable();
            $table->text('value')->nullable();
            $table->text('description')->nullable();
            $table->timestamp('accessed_at')->nullable();
            $table->index(['user_id', 'type', 'key', 'deleted_at']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_credentials');
    }
}
