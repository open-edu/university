<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMetadata extends Model
{
    protected $table = 'users_metadata';
}
