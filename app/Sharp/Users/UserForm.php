<?php

namespace App\Sharp\Users;

use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormWysiwygField;
use App\Models\User;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Form\Fields\SharpFormUploadField;
use Code16\Sharp\Form\Fields\SharpFormListField;
// use App\Sharp\Helpers\UploadFormatter; // USE CUSTOM FORMATTER
use Code16\Sharp\Form\Fields\SharpFormSelectField;
// use App\Models\Privileges;
use Illuminate\Support\Facades\Hash;

class UserForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;
    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        $user = $this->transform(User::findOrFail($id));

        $data = $this->setCustomTransformer(
            "password",
            function($value, $user, $attribute) {
                return '';
            })
            ->transform($user);

        return $data;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        $instance = $id ? User::findOrFail($id) : new User;

        if(!is_null($data['password'])){
            if (!Hash::check($data['password'], $instance['password'])) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }
        } else {
            unset($data['password']);
        }

        $this->save($instance, $data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        User::findOrFail($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("name")
                ->setLabel("Name")
        )->addField(
            SharpFormTextField::make("email")
                ->setLabel("Email")
        )->addField(
            SharpFormTextField::make("password")
                ->setLabel("Password")
                ->setInputTypePassword()
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column
            ->withSingleField("name")
            ->withSingleField("email")
            ->withSingleField("password");
        });
    }
}
