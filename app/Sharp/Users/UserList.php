<?php

namespace App\Sharp\Users;

use App\Models\User;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;

class UserList extends SharpEntityList
{
    /**
    * @return array
    */
    public function values()
    {
        //
    }

    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make("id")
                ->setLabel("ID")
        )->addDataContainer(
            EntityListDataContainer::make("name")
                ->setLabel("Name")
        )->addDataContainer(
            EntityListDataContainer::make("email")
                ->setLabel("Email")
        )->addDataContainer(
            EntityListDataContainer::make("admin_privilege")
                ->setLabel("Level")
        );
    }

    public function buildListConfig()
    {
        $this->setInstanceIdAttribute("id")
        ->setSearchable()
        ->setPaginated();
    }

    public function buildListLayout()
    {
        $this
            ->addColumn("id", 1)
            ->addColumn("name", 2)
            ->addColumn("email", 2);
    }

    public function getListData(EntityListQueryParams $params)
    {
        $users = User::orderBy('id', 'desc')->get();

        return $this->transform($users);
    }
}
